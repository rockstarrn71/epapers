<?php
  include("include/config.php");
  include("include/session.php");
  $cnn = new connection();
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - View News</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	  <link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
	  
     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        News
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
	  
        <div class="col-12">
		
        	<div class="box">
			
           <div class="col-12" style="border:1px solid #fbf1f1">
		   
				<div class="row">
					<div class="col-md-3">
                        <div class="form-group">
							<label>State</label>
							<select class="form-control" id="stateID" name="stateID">
							<option selected disabled>----- Select State -----</option>
							<?php
							$selectState = $cnn -> getrows("SELECT *FROM state_master");
							while($getState = mysqli_fetch_array($selectState))
							{
							?>
								<option value="<?php echo $getState['stateID']; ?>"><?php echo $getState['stateName']; ?></option>
							<?php } ?>
							</select>
						</div>
						<!-- /input-group -->
					</div>
					<!-- /.col-lg-6 -->
					<div class="col-md-3">
                        <div class="form-group">
							<label>City</label>
							<select class="form-control" id="cityID" name="cityID">
								<option selected disabled>----- Select City -----</option>
							</select>
						</div>
						<!-- /input-group -->
					</div>
					
					<div class="col-md-3">
                        <div class="form-group">
							<label>Date</label>
							<input type="text" id="datepicker" name="selectdate" class="form-control" placeholder="<?php echo date('m-d-Y');?>">
						</div>
						<!-- /input-group -->
					</div>
					<div class="col-md-2">
						<div class="form-group">
						<label></label>
							<button type="button" onclick="myFunction()" class="btn btn-primary" style="margin-top: 20%;">Search</button>
						</div>
						<!-- /input-group -->
					</div>
                <!-- /.col-lg-6 -->
              </div>
              <!-- /.row -->
		   </div>
            <!-- /.box-header -->
            <div class="box-body" id ="searchingdata">
			<?php 
			
			$selectState = $cnn -> getrows("SELECT *FROM state_master");
			
			if(mysqli_num_rows($selectState) !== 0)
			{								
				while($getState = mysqli_fetch_array($selectState))
				{
				 	$state_id = $getState['stateID'];


 			$dir = "/home/shreeji4036/public_html/e-paper/11-12-2017/".$getState['stateName'];
 			$files1 = scandir($dir);
    		if(isset($files1) && !empty($files1) && count($files1) > 0){
    			foreach($files1 as $key=>$val){
      
     			 $ext = pathinfo($val, PATHINFO_EXTENSION);
        
       			 if(!in_array($val,array(".","..")) && $ext == 'pdf'){
       			 $file = $dir."/".$val;
       			 		$file1 = str_replace("/home/shreeji4036/public_html","", $file);
 			?>

 			          			<h3><?php echo $getState['stateName']; ?></h3>
			<hr>  
		
 		<div class="row">
 			
       		<div class="col-xl-3 col-md-6 col-12">
	    		<div class="box-header with-border" style="background-color: #46be8a6b;padding: 7px;">
				<?php echo $getState['stateName']; ?>
            	</div>
            <div class="box box-default box-solid">
		   
			   <div class="info-box">
	            <a href="<?php echo $file1; ?>" target="_blank"><span class="info-box-icon bg-aqua"><i class="fa fa-info-circle"></i></span></a>
				<div class="info-box-content" style="margin-top: -9px;margin-left: 123px;">
	            <a onclick="return confirm('Are you sure You Want To Delete Data?')" href="addNewsScript.php?newsID=<?php echo $infos['newsID']?>">  <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span></a>
	            </div>
	            <!-- /.info-box-content -->
	          </div>
		    </div>
		       <!-- <div class="box-header with-border" style="background-color: #b1e4ce;margin-top: -50px;padding: 4px;">
				<center><h5 class="box-title" style="font-size: 17px;font-family: initial;"><?php echo $infos['date']; ?></h5></center>
			   
            </div> -->
          	</div>
          </div>
				<?php } } } } ?>
        <!-- /.col -->
      	
			<?php } else { ?>
				
					<center><h5 class="box-title" style="font-size: 17px;font-family: initial;color:red;">No News Uploaded!</h5></center>
			<?php }
			
			
			?>         
        </div>
    </div>
      <!-- /.row -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	

	<!-- DataTables -->
	<script src="assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>

	
	  
	  <script>
		  $( document ).ready(function() {
		  $( function() {
		    $( "#datepicker" ).datepicker({ minDate: -10, maxDate: "+0" });
		  } );
		  });
		  </script>
	<!-- This is data table -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- apro_admin for Data Table -->
	<script src="js/pages/data-table.js"></script>

	<script>
	$(document).ready(function(){
		$("#stateID").change(function(){
			var stateID = $(this).val();
			
			$.ajax({
            type: "POST",
            url: "getCity.php",
            data: {
				stateID : stateID  
			},
			success:function(data){
				$("#cityID").html(data);
			}
			});
		});
	});
	
		
	</script>
	<script>
	function myFunction() 
	{   
		var stateID = $('#stateID').val();
		var cityID= $('#cityID').val();
		var date= $('#datepicker').val();
		
		$.ajax
		 ({
			url: "testData1.php",
			type: 'POST',
			data:  {"stateID":stateID,"cityName":cityID,"date":date},
			 success: function(response) 
			{
				$("#searchingdata").html(response);
			}
		}); 
	}
	</script>
	
</body>

</html>
