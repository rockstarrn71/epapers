<?php
  include("include/config.php");
  include("include/session.php");
  $cnn = new connection();
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - City</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        City
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add City</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<form name="state" id="state" method="POST" action="addCityScript.php" enctype="multipart/form-data">

                	<div class="form-group row">
					    <label for="stateName" class="col-sm-2 col-form-label">Select State</label>
					    <div class="col-sm-10">
						    <select class="form-control" name="stateID" id="stateID" required>
								<option selected disabled>--- Select State ---</option>
								<?php 
								$selectState = $cnn -> getrows("SELECT *FROM state_master");
								while($getState = mysqli_fetch_array($selectState))
								{
								?>
	                    			<option value="<?php echo $getState['stateID']; ?>"><?php echo $getState['stateName']; ?></option>
	                    		<?php } ?>
	                  		</select>
					  	</div>
					</div>
					
		            	<div class="form-group row">
						  	<label for="cityName" class="col-sm-2 col-form-label">City Name</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="text" id="cityName" name="cityName" placeholder="City Name" required="">
						  	</div>
						</div>
					
					<div class="form-group row">
						  	<label for="image" class="col-sm-2 col-form-label">Image</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="file" id="image" name="image"  required="">
						  	</div>
						</div>
					<center>
						<button type="submit" id="addCity" name="addCity" class="btn btn-danger btn-flat" style="font-size: 14px;">Submit <i class="fa fa-fw fa-arrow-circle-o-right"></i></button>
					</center>
				</form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- /.row -->

      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">View City</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
				<thead>
					<tr>
						<th>No</th>
						<th>State Name</th>
						<th>City Name</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$selectState = $cnn -> getrows("SELECT state_master.*,city_master.* FROM city_master INNER JOIN state_master ON city_master.stateID = state_master.stateID");
						$i = 1;
						while($getCity = mysqli_fetch_array($selectState))
						{ $cityID = $getCity['cityID'];
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $getCity['stateName']; ?></td>
						<td><?php echo $getCity['cityName']; ?></td>
						<td><img src="<?php echo $getCity['image']; ?>" height="50" width="50"></td>
						<td>
							<a href="editCity.php?cityID=<?php echo $cityID; ?>" class="btn btn-danger btn-flat" style="font-size: 14px;">Edit <i class="fa fa-fw fa-arrow-circle-o-right"></i></a>
							<a href="addCityScript.php?cityID=<?php echo$cityID; ?>&deleteCity=deleteCity" class="btn btn-primary btn-flat" style="font-size: 14px;">Delete <i class="fa fa-fw fa-arrow-circle-o-right"></i></a>
						</td>
					</tr>
					<?php $i++; } ?>
					
				</tfoot>
			</table>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	<!-- Sparkline -->
	<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>

	<!-- This is data table -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- apro_admin for Data Table -->
	<script src="js/pages/data-table.js"></script>
	
	<script type="text/javascript">
	// $(document).ready(function(){
	// 	var maxField = 10; 
	// 	var addButton = $('.add_button'); 
	// 	var wrapper = $('.field_wrapper'); 
	// 	var fieldHTML = "";
	// 		fieldHTML += '<div class="form-group row">';
	// 					  	fieldHTML += '<label for="cityName" class="col-sm-2 col-form-label">City Name</label>';
	// 					 	fieldHTML += '<div class="col-sm-9">';
	// 							fieldHTML += '<input class="form-control" type="text" id="cityName" name="cityName[]" placeholder="City Name" required="">';
	// 					  	fieldHTML += '</div>';
	// 					  	fieldHTML += '<div class="col-sm-1">';
	// 					  		fieldHTML += '<a href="javascript:void(0);" class="form-control btn btn-danger btn-flat remove_button"  title="Remove field"><i class="fa fa-minus" aria-hidden="true"></i></a>';
	// 						fieldHTML += '</div>';
	// 					fieldHTML += '</div>'; 

	// 	var x = 1; 
	// 	$(addButton).click(function(){ 
	// 		if(x < maxField){
	// 			x++; 
	// 			$(wrapper).append(fieldHTML); 
	// 		}
	// 	});
	// 	$(wrapper).on('click', '.remove_button', function(e){ 
	// 		e.preventDefault();
	// 		$('.col-sm-9').parent('div').remove(); 
	// 		x--; 
	// 	});
	// });
</script>
</body>
</html>
