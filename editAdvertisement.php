<?php
  include("include/config.php");
  include("include/session.php");
  $cnn = new connection();
  $adID = $_GET['adID'];
  $selectAD = $cnn -> getrows("SELECT *FROM ad_master WHERE adID = '$adID'");
  $getAD = mysqli_fetch_array($selectAD);
  
   
  
  
  
  
  //echo $getAD['url'];
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - Edit Advertisement</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Advertisement
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Advertisement</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<form name="ad" id="ad" method="POST" action="addAdvertisementScript.php" enctype="multipart/form-data">
            		<input class="form-control" type="hidden" id="adID" name="adID" value="<?php echo $adID; ?>">
	            	<div class="form-group row">
					  <label for="adTitle" class="col-sm-2 col-form-label">Title</label>
					  <div class="col-sm-6">
						<input class="form-control" type="text" id="adTitle" name="adTitle" value="<?php echo $getAD['adTitle']; ?>" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="adSTitle" class="col-sm-2 col-form-label">Sub Title</label>
					  <div class="col-sm-6">
						<input class="form-control" type="text" id="adSTitle" name="adSTitle" value="<?php echo $getAD['adSTitle']; ?>" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="adDescription" class="col-sm-2 col-form-label">Description</label>
					  <div class="col-sm-6">
						<input class="form-control" type="text" id="adDescription" name="adDescription" value="<?php echo $getAD['adDescription']; ?>" required>
					  </div>
					</div>
					
					
					<div class="form-group row">
					  <label for="adPhoto" class="col-sm-2 col-form-label">Photo</label>
					  <div class="col-sm-6">
						<input class="form-control" type="file" id="adPhoto" name="adPhoto">
					<a href="<?php echo $getAD['url']; ?>" target="_blank"><img src="<?php echo $getAD['adPhoto']; ?>" width="50" height="50" ></a>
					 </div>
					</div>
					
					
					<!--<div class="form-group row">
					  <label for="adPhoto" class="col-sm-2 col-form-label">URL Link</label>
					  <div class="col-sm-6">
						<input class="form-control" type="text" id="url" name="url" value="<?php echo $getAD['url']; ?>" required>
					 </div>
					</div>
					
					
					
					<div class="form-group row">
					  <label for="adPhoto" class="col-sm-2 col-form-label">Link</label>
					  <div class="col-sm-6">
					      <a href="<?php echo $getAD['url']; ?>">Link</a>
					 </div>
					</div>-->
					
					
					<div class="form-group row">
					  <label for="adFromDate" class="col-sm-2 col-form-label">From Date</label>
					  <div class="col-sm-6">
						<input class="form-control" type="date" id="adFromDate" name="adFromDate" value="<?php echo $getAD['adFromDate']; ?>" required>
					  </div>
					</div>
					<div class="form-group row">
					  <label for="adToDate" class="col-sm-2 col-form-label">To Date</label>
					  <div class="col-sm-6">
						<input class="form-control" type="date" id="adToDate" name="adToDate" value="<?php echo $getAD['adToDate']; ?>" required>
					  </div>
					</div>
					<center>
						<button type="submit" id="editAD" name="editAD" class="btn btn-danger btn-flat" style="font-size: 14px;margin-right: 18%;">Update <i class="fa fa-fw fa-arrow-circle-o-right"></i></button>
					</center>
				</form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- /.row -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	
	
	<!-- Sparkline -->
	<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>
	
</body>

</html>
