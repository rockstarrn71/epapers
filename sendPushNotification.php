<?php
include("include/config.php");
include("include/session.php");
$cnn = new connection();
$flashMessage = $cnn -> getrows("SELECT c.* FROM configs c WHERE c.config_key = 'flash_message' LIMIT 1");
$getFlashMessage = mysqli_fetch_array($flashMessage);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - Send Notification</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

    <!-- font awesome -->
    <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">

    <!-- ionicons -->
    <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">

    <!-- theme style -->
    <link rel="stylesheet" href="css/master_style.css">

    <!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.css">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">


</head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

    <!-- header -->
    <?php include("include/header.php"); ?>
    <!-- End header -->

    <!-- Left side column. contains the logo and sidebar -->
    <?php include("include/leftbar.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Send Notification
                <small>Control panel</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <!-- Basic Forms -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Send Notification</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-12">
                            <form name="flash_message_form" id="flash_message_form" method="POST" action="sendPushNotificationScript.php" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input class="form-control" type="text" id="notification_text" name="notification_text"
                                               placeholder="Notification Text" value="" required>
                                    </div>
                                    <!----Adding new content --->
                                   <!-- 
                                    <br><br><br><br>
                                     <div class="col-sm-12">
                                        <input class="form-control" type="text" id="title" name="title" placeholder="Title">
                                    </div>
                                    
                                     <br><br><br><br>
                                     <div class="col-sm-12">
                                        <select class="form-control" type="text" id="tab" name="tab">
                                            <option value="">--Select Tab---</option>
                                            <option value="newspaper">News Papers</option>
                                            <option value="status">Status Saver</option>
                                            <option value="imgorvid">Images Or Videos</option>
                                            </select>
                                    </div> 
                                    
                                    <br><br><br><br>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="file" id="image" name="image">
                                    </div>
                                    
                                     -->
                                    
                                    <!----Adding new content --->
                                </div>
                                <center>
                                    <button type="submit" id="SendNotification" name="SendNotification" class="btn btn-danger btn-flat"
                                            style="font-size: 14px;">Send Notification<i
                                            class="fa fa-fw fa-arrow-circle-o-right"></i></button>
                                </center>
                            </form>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include("include/footer.php"); ?>

    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap 4.0-->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>


<!-- Sparkline -->
<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>


<!-- Slimscroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- apro_admin App -->
<script src="js/template.js"></script>

<!-- apro_admin for demo purposes -->
<script src="js/demo.js"></script>

<!-- This is data table -->
<script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

<!-- start - This is for export functionality only -->
<script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- apro_admin for Data Table -->
<script src="js/pages/data-table.js"></script>
<script type="text/javascript">
    WeatherIcon.add('icon1', WeatherIcon.SLEET, {stroke: false, shadow: false, animated: true});
    WeatherIcon.add('icon2', WeatherIcon.SNOW, {stroke: false, shadow: false, animated: true});
    WeatherIcon.add('icon3', WeatherIcon.LIGHTRAINTHUNDER, {stroke: false, shadow: false, animated: true});
</script>


</body>

</html>
