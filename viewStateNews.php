<?php 
	include("include/config.php");
	include("include/session.php");
 	$cnn = new connection();
	$stateID = $_POST["stateID"];
	$supID = $_POST["supID"];
 	$NewDate = Date('Y-m-d', strtotime("-10 days"));
 	$current_date = date('Y-m-d');
	$date1 = $_POST["date"];
	if($date1 == '')
	{
		$date = '';
	}
	else
	{
		$date = date("Y-m-d", strtotime($date1));
	}
 	if($stateID != '' && $supID == '' && $date == '')
	{
		$selectState = $cnn -> getrows("SELECT distinct state_master.stateName,state_master.stateID FROM supnews_master left join state_master on supnews_master.stateID=state_master.stateID where supnews_master.stateID='$stateID' and supnews_master.snuploadDate >='$NewDate' and supnews_master.snuploadDate <='$current_date'");
			
			if(mysqli_num_rows($selectState) !== 0)
			{				
			while($getState = mysqli_fetch_array($selectState))
			{ ?>
          	<h3><?php echo $getState['stateName']; ?></h3>
			<hr>  
		
 			<div class="row">
 			<?php 
 			$info = $cnn -> getrows("SELECT sup_master.*,supnews_master.* from supnews_master inner join sup_master on supnews_master.supID=sup_master.supID where supnews_master.stateID='$stateID' and  supnews_master.snuploadDate >='$NewDate' and supnews_master.snuploadDate <='$current_date' order by supnews_master.snuploadDate desc");
			
				while($infos = mysqli_fetch_assoc($info))
				{ ?>
       		<div class="col-xl-3 col-md-6 col-12">
	    	<div class="box-header with-border" style="background-color: #46be8a6b;padding: 7px;">
			<center>
              <h3 class="box-title"  style="font-size: 19px;font-family: initial;" ><?php echo $infos['supName']; ?></h3>
			</center>   
            </div>
          	<div class="box box-default box-solid">
		   
		   	<div class="info-box">
            <a href="supplementpdf/<?php echo $infos['snPdf']; ?>" target="_blank"><span class="info-box-icon bg-aqua"><i class="fa fa-info-circle"></i></span></a>
			 <div class="info-box-content" style="margin-top: -9px;margin-left: 123px;">
            <a onclick="return confirm('Are you sure You Want To Delete Data?')" href="addSupplementNewsScript.php?snID=<?php echo $infos['snID']?>">  <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span></a>
            </div>
            <!-- /.info-box-content -->
          	</div>
		
		    </div>
			<div class="box-header with-border" style="background-color: #b1e4ce;margin-top: -50px;padding: 4px;">
				<center><h5 class="box-title" style="font-size: 17px;font-family: initial;"><?php echo date('d M, Y', strtotime($infos['snUploadDate'])); ?></h5></center> 
            </div>
		
          </div>
				<?php } ?>
        <!-- /.col -->
      	</div>
			<?php } } else { ?>
					<center><h5 class="box-title" style="font-size: 17px;font-family: initial;color:red;">No News Uploaded!</h5></center>
			<?php } }	
		else if($stateID != '' && $supID != '' && $date == '')
		{
			$selectState = $cnn -> getrows("SELECT distinct state_master.stateName,state_master.stateID FROM supnews_master left join state_master on supnews_master.stateID=state_master.stateID where supnews_master.stateID='$stateID' and supnews_master.snuploadDate >='$NewDate' and supnews_master.snuploadDate <='$current_date' and supnews_master.supID ='$supID'");
			if(mysqli_num_rows($selectState) !== 0)
			{								
			while($getState = mysqli_fetch_array($selectState))
			{
							?>
          <h3><?php echo $getState['stateName']; ?></h3>
		<hr>  
		
 		<div class="row">
 		<?php 
 		$info = $cnn -> getrows("SELECT sup_master.*,supnews_master.* from supnews_master inner join sup_master on supnews_master.stateID=sup_master.stateID where supnews_master.stateID='$stateID' and  supnews_master.snuploadDate >='$NewDate' and supnews_master.snuploadDate <='$current_date' and sup_master.supID ='$supID' order by supnews_master.snuploadDate desc");
			
				while($infos = mysqli_fetch_assoc($info))
				{ ?>
        <div class="col-xl-3 col-md-6 col-12">
	    <div class="box-header with-border" style="background-color: #46be8a6b;padding: 7px;">
		<center>
              <h3 class="box-title"  style="font-size: 19px;font-family: initial;" ><?php echo $infos['supName']; ?></h3>
			</center>   
            </div>
          <div class="box box-default box-solid">
		   
		   <div class="info-box">
            <a href="supplementpdf/<?php echo $infos['snPdf']; ?>" target="_blank"><span class="info-box-icon bg-aqua"><i class="fa fa-info-circle"></i></span></a>
			 <div class="info-box-content" style="margin-top: -9px;margin-left: 123px;">
            <a onclick="return confirm('Are you sure You Want To Delete Data?')" href="addSupplementNewsScript.php?snID=<?php echo $infos['snID']?>">  <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span></a>
            </div>
            <!-- /.info-box-content -->
          </div>
		
		    </div>
			<div class="box-header with-border" style="background-color: #b1e4ce;margin-top: -50px;padding: 4px;">
				<center><h5 class="box-title" style="font-size: 17px;font-family: initial;"><?php echo date('d M, Y', strtotime($infos['snUploadDate'])); ?></h5></center>
            </div>
            </div>
				<?php } ?>
        <!-- /.col -->
      </div>
		<?php } } else { ?>
					<center><h5 class="box-title" style="font-size: 17px;font-family: initial;color:red;">No News Uploaded!</h5></center>
<?php } } 
else if($stateID != '' && $supID != '' && $date !== '')
		{
			$selectState = $cnn -> getrows("SELECT distinct state_master.stateName,state_master.stateID FROM supnews_master left join state_master on supnews_master.stateID=state_master.stateID where supnews_master.stateID='$stateID' and supnews_master.snuploadDate ='$date' and supnews_master.snuploadDate ='$date' and supnews_master.supID ='$supID'");
			if(mysqli_num_rows($selectState) !== 0)
			{								
			while($getState = mysqli_fetch_array($selectState))
			{
							?>
          <h3><?php echo $getState['stateName']; ?></h3>
		<hr>  
		
 		<div class="row">
 		<?php 
 		$info = $cnn -> getrows("SELECT sup_master.*,supnews_master.* from supnews_master inner join sup_master on supnews_master.stateID=sup_master.stateID where supnews_master.stateID='$stateID' and  supnews_master.snuploadDate ='$date' and supnews_master.snuploadDate ='$date' and sup_master.supID ='$supID' order by supnews_master.snuploadDate desc");
			
				while($infos = mysqli_fetch_assoc($info))
				{ ?>
        <div class="col-xl-3 col-md-6 col-12">
	    <div class="box-header with-border" style="background-color: #46be8a6b;padding: 7px;">
		<center>
              <h3 class="box-title"  style="font-size: 19px;font-family: initial;" ><?php echo $infos['supName']; ?></h3>
			</center>   
            </div>
          <div class="box box-default box-solid">
		   
		   <div class="info-box">
            <a href="supplementpdf/<?php echo $infos['snPdf']; ?>" target="_blank"><span class="info-box-icon bg-aqua"><i class="fa fa-info-circle"></i></span></a>
			 <div class="info-box-content" style="margin-top: -9px;margin-left: 123px;">
            <a onclick="return confirm('Are you sure You Want To Delete Data?')" href="addSupplementNewsScript.php?snID=<?php echo $infos['snID']?>">  <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span></a>
            </div>
            <!-- /.info-box-content -->
          </div>
		
		    </div>
			<div class="box-header with-border" style="background-color: #b1e4ce;margin-top: -50px;padding: 4px;">
				<center><h5 class="box-title" style="font-size: 17px;font-family: initial;"><?php echo date('d M, Y', strtotime($infos['snUploadDate'])); ?></h5></center>
            </div>
            </div>
				<?php } ?>
        <!-- /.col -->
      </div>
		<?php } } else { ?>
					<center><h5 class="box-title" style="font-size: 17px;font-family: initial;color:red;">No News Uploaded!</h5></center>
<?php } } 

if($stateID != '' && $supID == '' && $date !== '')
	{
		$selectState = $cnn -> getrows("SELECT distinct state_master.stateName,state_master.stateID FROM supnews_master left join state_master on supnews_master.stateID=state_master.stateID where supnews_master.stateID='$stateID' and supnews_master.snuploadDate ='$date' and supnews_master.snuploadDate ='$date'");
			
			if(mysqli_num_rows($selectState) !== 0)
			{				
			while($getState = mysqli_fetch_array($selectState))
			{ ?>
          	<h3><?php echo $getState['stateName']; ?></h3>
			<hr>  
		
 			<div class="row">
 			<?php 
 			$info = $cnn -> getrows("SELECT sup_master.*,supnews_master.* from supnews_master inner join sup_master on supnews_master.supID=sup_master.supID where supnews_master.stateID='$stateID' and  supnews_master.snuploadDate ='$date' and supnews_master.snuploadDate ='$date' order by supnews_master.snuploadDate desc");
			
				while($infos = mysqli_fetch_assoc($info))
				{ ?>
       		<div class="col-xl-3 col-md-6 col-12">
	    	<div class="box-header with-border" style="background-color: #46be8a6b;padding: 7px;">
			<center>
              <h3 class="box-title"  style="font-size: 19px;font-family: initial;" ><?php echo $infos['supName']; ?></h3>
			</center>   
            </div>
          	<div class="box box-default box-solid">
		   
		   	<div class="info-box">
            <a href="supplementpdf/<?php echo $infos['snPdf']; ?>" target="_blank"><span class="info-box-icon bg-aqua"><i class="fa fa-info-circle"></i></span></a>
			 <div class="info-box-content" style="margin-top: -9px;margin-left: 123px;">
            <a onclick="return confirm('Are you sure You Want To Delete Data?')" href="addSupplementNewsScript.php?snID=<?php echo $infos['snID']?>">  <span class="info-box-icon bg-red"><i class="fa fa-trash-o"></i></span></a>
            </div>
            <!-- /.info-box-content -->
          	</div>
		
		    </div>
			<div class="box-header with-border" style="background-color: #b1e4ce;margin-top: -50px;padding: 4px;">
				<center><h5 class="box-title" style="font-size: 17px;font-family: initial;"><?php echo date('d M, Y', strtotime($infos['snUploadDate'])); ?></h5></center> 
            </div>
		
          </div>
				<?php } ?>
        <!-- /.col -->
      	</div>
			<?php } } else { ?>
					<center><h5 class="box-title" style="font-size: 17px;font-family: initial;color:red;">No News Uploaded!</h5></center>
			<?php } }



?>			