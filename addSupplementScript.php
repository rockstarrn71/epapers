<?php
	include("include/config.php"); 
	include("include/session.php"); 
	$cnn = new connection();

	if(isset($_POST['addSup']))
	{
		$stateID = $_POST['stateID'];
		$supName = $_POST['supName'];

		$insertSP = array("stateID" => $stateID, "supName" => $supName);
		$insertData = $cnn -> insertRec($insertSP, "sup_master");

		if($insertData)
		{
			header("Location: addSupplement.php");
		}
	}
	if(isset($_GET['deleteSup']))
	{
		$supID = $_GET['supID'];

		$deleteSP = $cnn -> updatedeleterows("DELETE FROM sup_master WHERE supID = '$supID'");

		if($deleteSP)
		{
			header("Location: viewSupplement.php");
		}	
	}
	if(isset($_POST['editSup']))
	{
		$supID = $_POST['supID'];
		$stateID = $_POST['stateID'];
		$supName = $_POST['supName'];

		$updateSup = $cnn -> updatedeleterows("UPDATE sup_master SET stateID = '$stateID',supName = '$supName' WHERE supID = '$supID'");

		if($updateSup)
		{
			header("Location: viewSupplement.php");
		}
	}
?>