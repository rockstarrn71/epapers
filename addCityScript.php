<?php
	include("include/config.php"); 
	include("include/session.php"); 
	$cnn = new connection();

	if(isset($_POST['addCity']))
	{
		$stateID = $_POST['stateID'];
		$cityName = $_POST['cityName'];
		$image = time().$_FILES['image']['name'];

		$path = "images/cat_image/".$image;
		
		$insertCT = array("stateID" => $stateID, "cityName" => $cityName, "image" => $path,"cityStatus"=>0);
		$insertData = $cnn -> insertRec($insertCT, "city_master");
		
		$path1 = "images/cat_image/";
		move_uploaded_file($_FILES['image']['tmp_name'], $path1.$image);

		if($insertData)
		{
			header("Location: addCity.php");
		}
	}
	if(isset($_GET['deleteCity']))
	{
		$cityID = $_GET['cityID'];

		$deleteCT = $cnn -> updatedeleterows("DELETE FROM city_master WHERE cityID = '$cityID'");

		if($deleteCT)
		{
			header("Location: addCity.php");
		}	
	}
	if(isset($_POST['editCity']))
	{
		$cityID = $_POST['cityID'];
		$stateID = $_POST['stateID'];
		$cityName = $_POST['cityName'];
		$image = time().$_FILES['image']['name'];

		if($_FILES['image']['name'] != "")
		{
			$path = "images/cat_image/".$image;

			$updateState = $cnn -> updatedeleterows("UPDATE city_master SET stateID = '$stateID', cityName = '$cityName', image = '$path' WHERE cityID = '$cityID'");

			$path1 = "images/cat_image/";
			move_uploaded_file($_FILES['image']['tmp_name'], $path1.$image);
		}
		else
		{
			$updateState = $cnn -> updatedeleterows("UPDATE city_master SET stateID = '$stateID', cityName = '$cityName' WHERE cityID = '$cityID'");
		}

		if($updateState)
		{
			header("Location: addCity.php");
		}
	}
?>