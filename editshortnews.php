<?php
   
  include("include/session.php");
 $conn = mysqli_connect("localhost", "ashvitht_enewson", "Ashvith@99", "ashvitht_enews");
  $id = $_REQUEST['id'];
  $sql = "select * from shortnews where id = '".$id."'";
  
  $res = mysqli_query($conn,$sql);
  while($row = mysqli_fetch_array($res))
  {
  
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - Dashboard</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        State
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Edit City</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<form name="state" id="state" method="POST" action="editshortnews.php" enctype="multipart/form-data">
            		<input class="form-control" type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>">


                   <div class="form-group row">
					  <label for="cityName" class="col-sm-2 col-form-label">News Title</label>
					  <div class="col-sm-10">
						<input class="form-control" type="text" id="name" name="title" value="<?php echo $row['title']; ?>">
					  </div>
					</div>



            		<div class="form-group row">
					    <label for="stateName" class="col-sm-2 col-form-label">Category Selection</label>
					    <div class="col-sm-10">
						    <select class="form-control" name="categories" id="categories" required>
								<?php
								$sqll = "select * from news_categories";
								$ress = mysqli_query($conn,$sqll);
								while($red = mysqli_fetch_assoc($ress))
								{
								    if($red['id'] == $row['categories'])
								    {
								        ?>
								<option value="<?php echo $red['id']; ?>" selected><?php echo $red['name']; ?></option>        
								        
								        <?php
								    }
								    else
								    {
								        ?>
								       
								       <option value="<?php echo $red['id']; ?>" selected><?php echo $red['name']; ?></option> 
								    <?php
								    }
								}
		
								
								?>
	                  		</select>
					  	</div>
					</div>
	            	
	            	
	            	<div class="form-group row">
					    <label for="stateName" class="col-sm-2 col-form-label">Sub Category Selection</label>
					    <div class="col-sm-10">
						    <select class="form-control" name="subcat" id="subcat" required>
								<?php
								$sqll = "select * from news_categories";
								$ress = mysqli_query($conn,$sqll);
								while($red = mysqli_fetch_assoc($ress))
								{
								    if($red['id'] == $row['subcat'])
								    {
								        ?>
								<option value="<?php echo $red['id']; ?>" selected><?php echo $red['name']; ?></option>        
								        
								        <?php
								    }
								    else
								    {
								        ?>
								       
								       <option value="<?php echo $red['id']; ?>"><?php echo $red['name']; ?></option> 
								    <?php
								    }
								}
		
								
								?>
	                  		</select>
					  	</div>
					</div>
	            	
	            	
	            	
	            	
	            	
	            	
	            	<div class="form-group row">
						  	<label for="image" class="col-sm-2 col-form-label">News Description</label>
						 	<div class="col-sm-10">
								<textarea name="description" class="form-control" id="description" rows="5"><?php echo $row['description']; ?></textarea>
						  	</div>
					</div>
	            	
	            	
					<div class="form-group row">
					  <label for="image" class="col-sm-2 col-form-label">Image</label>
					  <div class="col-sm-10">
						<input class="form-control" type="file" id="image" name="image">
						<img src="<?php echo $row['image']; ?>" height="50" width="50">
					  </div>
					</div>
					
					
					<br><br><br>
					
					<center>
						
						<input type="submit" id="edit" name="edit" value="Edit Data" class="btn btn-success">
					</center>
					</form>
					<br>
					<br>
					
					
										
					
					
					
				
				
  <?php
  }
  ?>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- /.row -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	
	
	<!-- Sparkline -->
	<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>
	
</body>

</html>

<?php

include "connection.php";

if(isset($_POST['edit']))
{

$id = $_POST['id'];
$title = $_POST['title'];
$categories = $_POST['categories'];
$description = $_POST['description'];

$image = time().$_FILES['image']['name'];
		

		
 
 
        


if($_FILES['image']['name'] != '')
{
    $path = "shortnews/".$image;
    $sql = "update shortnews set title = '".$title."',categories='".$categories."',description = '".$description."',image = '".$path."' where id ='".$id."' ";
    // image storing in folder
   $path1 = "shortnews/";
   move_uploaded_file($_FILES['image']['tmp_name'], $path1.$image);

}
else
{
    $sql = "update shortnews set title = '".$title."',categories='".$categories."',description = '".$description."'  where id ='".$id."' ";
}


$res = mysqli_query($conn,$sql);





if($res)
{
    echo "<script>alert('Updated Successfully');window.location='shortnews.php';</script>";
}
else
{
    echo "<script>alert('Not Updated');window.location='shortnews.php';</script>";
}





}


?>