<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="image float-left">
                <img src="images/user.jpg" class="rounded-circle" alt="User Image">
            </div>
            <div class="info float-left">
                <p><?php echo $_SESSION['username']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="active">
                <a href="index.php">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>
            </li>
            <li>
                <a href="flashMessage.php">
                    <i class="fa fa-bell-o"></i> <span>Flash Message</span>

                </a>
            </li>
            <li>
                <a href="sendPushNotification.php">
                    <i class="fa fa-bell-o"></i> <span>Send Notification</span>

                </a>
            </li>
            
            
            <li>
                <a href="push.php">
                    <i class="fa fa-bell-o"></i> <span>Send Notification New</span>

                </a>
            </li>

            <li class="">
                <a href="addState.php">
                    <i class="fa fa-th"></i>
                    <span>State</span>

                </a>

            </li>

            <li class="">
                <a href="addCity.php">
                    <i class="fa fa-th"></i>
                    <span>City</span>

                </a>
                <!--<ul class="treeview-menu">
                  <li><a href="addCity.php">Add City</a></li>
                  <li><a href="viewCity.php">View City</a></li>
                </ul>-->
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>State News</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <!-- <li><a href="addNews.php">Add News</a></li> -->
                    <li><a href="viewStateCityNews.php">View News</a></li>
                </ul>
            </li>

            <!-- <li class="treeview">
             <a href="#">
               <i class="fa fa-th"></i>
               <span>State Supplements</span>
               <span class="pull-right-container">
                 <i class="fa fa-angle-left pull-right"></i>
               </span>
             </a>
             <ul class="treeview-menu">
               <li><a href="addSupplement.php">Add Supplement</a></li>
               <li><a href="viewSupplement.php">View Supplement</a></li>
               <li><a href="addSupplementNews.php">Add Supplement News</a></li>
               <li><a href="viewSupplementNews.php">View Supplement News</a></li>
             </ul>
           </li> -->

            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>City Supplements</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="addCitySupplement.php">Add Supplement</a></li>
                <li><a href="viewCitySupplement.php">View Supplement</a></li>
                <li><a href="addCitySupplementNews.php">Add Supplement News</a></li>
                <li><a href="viewCitySupplementNews.php">View Supplement News</a></li>
              </ul>
            </li>   -->

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Advertisement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="addAdvertisement.php">Add Advertisement</a></li>
                    <li><a href="viewAdvertisement.php">View Advertisement</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Other news</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="addOthernews.php">Add Othernews</a></li>
                    <li><a href="viewOthernews.php">View Othernews</a></li>
                    
                </ul>
            </li>
            
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Add Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <!--<li><a href="add_categories.php">Add Short News Categories</a></li>-->
                    <li><a href="add_news_categories.php">Add News Categories</a></li>
                    <li><a href="add_media_category.php">Add Media Categories</a></li>
                   <li><a href="gallery_categories.php">Add Gallery Categories</a></li>
                    
                </ul>
            </li>
            
           <!-- <li class="">
                <a href="add_categories.php">
                    <i class="fa fa-th"></i>
                    <span>Add Categories</span>

                </a>

            </li>-->
            
             <li class="">
                <a href="shortnews.php">
                    <i class="fa fa-th"></i>
                    <span>Daily News</span>

                </a>

            </li>
            
            
            
            
            <!-- add gallery -->
            
            
            <!--<li class="">
                <a href="add_news.php">
                    <i class="fa fa-th"></i>
                    <span>Gallery</span>

                </a>

            </li>-->
            
            <li class="">
                <a href="addgallery.php">
                    <i class="fa fa-th"></i>
                    <span>Gallery</span>

                </a>

            </li>
            
            <li class="">
                <a href="media.php">
                    <i class="fa fa-th"></i>
                    <span>Video</span>

                </a>

            </li>
            
            
            
            <!-- add gallery --->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Contact</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="viewContact.php">View Contacts</a></li>
                </ul>
            </li>
            
            <li>
                <a href="version.php">
                    <i class="fa fa-bell-o"></i> <span>Version</span>

                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
    <!--div class="sidebar-footer">
		<!-- item-->
    <!-- <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="fa fa-cog fa-spin"></i></a> -->
    <!-- item-->
    <!-- <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="fa fa-envelope"></i></a> -->
    <!-- item-->
    <!--a href="logout.php" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
</div-->
</aside>