<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $headers = array("Content-Type:multipart/form-data");
 
    $Title = $_REQUEST["Title"];
    $Description = $_REQUEST["Description"];
    $Type = $_REQUEST["Type"];
    $Date = $_REQUEST["Date"];
    $email = $_REQUEST["email"];
    $mobile = $_REQUEST["mobile"];
    $username = $_REQUEST["username"];
    //$fileUpload = $_FILES['fileUpload']['name'];
	$status = 1;

	if($Type == 'image')
	{
		$image = rand().$_FILES['fileUpload']['name'];

		$insertNews = array("email"=>$email, "mobile"=>$mobile, "username"=>$username, "Title"=>$Title, "Description"=>$Description, "Type"=> $Type, "Date"=>$Date, "fileUpload"=>$image, "status"=>$status);
		
		$news = $cnn -> insertRec($insertNews, "Othernews_master");

		$path = "../othernewsimage/";
		move_uploaded_file($_FILES['fileUpload']['tmp_name'], $path.$image);
		if($news > 0)
		{		
			echo json_encode(array("ResponseCode"=>"1","ResponseMsg"=> "News Insert Successfully.","Result"=>"true"));
		}
		else
		{
			echo json_encode(array("ResponseCode"=>"2","ResponseMsg"=> "Could Not Insert News.","Result"=>"true"));
		}
	}
	if($Type == 'video')
	{
		$video = rand().$_FILES['fileUpload']['name'];

		$insertNews = array("email"=>$email, "mobile"=>$mobile, "username"=>$username, "Title"=>$Title, "Description"=>$Description, "Type"=> $Type, "Date"=>$Date, "fileUpload"=>$video, "status"=>$status);
		
		$news = $cnn -> insertRec($insertNews, "Othernews_master");

		$path = "../othernewsvideo/";
		move_uploaded_file($_FILES['fileUpload']['tmp_name'], $path.$video);
		if($news > 0)
		{		
			echo json_encode(array("ResponseCode"=>"1","ResponseMsg"=> "News Insert Successfully.","Result"=>"true"));
		}
		else
		{
			echo json_encode(array("ResponseCode"=>"2","ResponseMsg"=> "Could Not Insert News.","Result"=>"true"));
		}
	}
?>