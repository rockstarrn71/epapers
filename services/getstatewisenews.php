<?php
    header('Access-Control-Allow-Origin: *');
    error_reporting(E_ALL);
    include("../include/config.php");
    $cnn = new connection();
    $stateID = $_REQUEST['stateID'];
    $token = $_REQUEST['token'];
    $appid = $_REQUEST['appId'];
    $date1 = date('Y-m-d');
    $date = date('d-m-Y', strtotime($date1));
        
    $list = array();
    $paper=array();
    $city = array();
    
    $selectState1 = $cnn -> countrow("SELECT * FROM state_master where stateID = '$stateID'");
    
    if($token)
    	{
    		if($appid)
	    	{
	    		if($appid == "com.teluguenewspaperspdf.dailynewspaper")
		    	{
		    		$countToken = $cnn-> countrow("SELECT *FROM  device_token1 WHERE token = '$token'");
		    		if($countToken == 0)
    				{
    					$insertToken = array("token"=>$token);
	            		 	$AddToken = $cnn -> insertRec($insertToken, "device_token1");
    				}
		    		
		    	}
		    	else
		    	{
		    		$countToken = $cnn-> countrow("SELECT *FROM  device_token WHERE token = '$token'");
		    		if($countToken == 0)
    				{
    					$insertToken = array("token"=>$token);
	            		 	$AddToken = $cnn -> insertRec($insertToken, "device_token");
    				}
		    	}
		    	
	    	}
	    	else
		{
		   	$countToken = $cnn-> countrow("SELECT *FROM  device_token WHERE token = '$token'");
		    	if($countToken == 0)
    			{
    				$insertToken = array("token"=>$token);
	            		$AddToken = $cnn -> insertRec($insertToken, "device_token");
    			}
		}
        }
    if($selectState1 > 0)
    {
        $selectState = $cnn -> getrows("SELECT * FROM state_master where stateID = '$stateID'");
        while($getState = mysqli_fetch_assoc($selectState))
        {
            $stateName = $getState['stateName'];
            $selectCity = $cnn -> getrows("SELECT *FROM city_master WHERE stateID = '$stateID' ORDER BY cityID ASC");
            while ($getCity = mysqli_fetch_assoc($selectCity))
            {
                $cityName = $getCity['cityName'];
                $dir = "/home/ashvithtechlabs/public_html/epapersnews/".$date."/".$stateName."/".$cityName;
                if(file_exists($dir))
                {
                    $files1 = scandir($dir);
                    if(isset($files1) && !empty($files1) && count($files1) > 0)
                    {
                        $ar=0;
                        $paper=array();
                        foreach($files1 as $key=>$val)
                        {
                            $ext = pathinfo($val, PATHINFO_EXTENSION);
                            if(!in_array($val,array(".","..")) && $ext == 'pdf')
                            {
                                $file = $dir."/".$val;
                                $paper[$ar]['pdf_url'] = str_replace("/home/ashvithtechlabs/public_html/epapersnews/","", $file);
                                $thumb = str_replace(".pdf",".jpg", $paper[$ar]['pdf_url']);
                                if(file_exists('../'.$thumb)){
                                    $paper[$ar]['thumb_url'] = $thumb;
                                }else{
                                    $paper[$ar]['thumb_url'] = $getCity['image'];
                                }
                                $ar++;
                            }
                        }
                        if($paper != null)
                        {
                            $list["pdf"] = $paper;  
                        }
                        else
                        {
                            $list["pdf"] = [];  
                        }
                        
                    }    
                }
                $list["cityName"] = $cityName;
                $city[] = $list;
            }
        }
        echo json_encode(array("city" => $city, "ResponseCode" => "1", "Result" => "True"));
    }
    else
    {
        echo json_encode(array("city" => [], "ResponseCode" => "2", "Result" => "False"));
    }
?>