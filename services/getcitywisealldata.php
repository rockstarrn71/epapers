<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $stateID = $_REQUEST['stateID'];
    $cityName = $_REQUEST['cityName'];
    $date1 = date('Y-m-d');
    $date = date('d-m-Y', strtotime($date1));
        
    $list = array();
    $paper=array();
    $city = array();
    
    $selectState1 = $cnn -> countrow("SELECT * FROM state_master where stateID = '$stateID'");
    if($selectState1 > 0)
    {
        $selectState = $cnn -> getrows("SELECT * FROM state_master where stateID = '$stateID'");
        $getState = mysqli_fetch_assoc($selectState);
        
            $stateName = $getState['stateName'];
            $selectCity1 = $cnn -> countrow("SELECT *FROM city_master WHERE stateID = '$stateID' AND cityName = '$cityName'");
            if($selectCity1 < 0)
            {
                echo json_encode(array("pdf" => $city, "ResponseCode" => "3", "Result" => "True"));
            }
            else
            {
                
                $selectCity = $cnn -> getrows("SELECT *FROM city_master WHERE stateID = '$stateID' AND cityName = '$cityName'");
                $getCity = mysqli_fetch_assoc($selectCity);
            
                $cityName = $getCity['cityName'];
                $dir = "/home/ashvithtechlabs/public_html/epapersnews/".$date."/".$stateName."/".$cityName;
                
                if(file_exists($dir))
                {
                    $files1 = scandir($dir);
                    if(isset($files1) && !empty($files1) && count($files1) > 0)
                    {
                        $ar=0;
                        $paper=array();
                        foreach($files1 as $key=>$val)
                        {
                            $ext = pathinfo($val, PATHINFO_EXTENSION);
                            if(!in_array($val,array(".","..")) && $ext == 'pdf')
                            {
                                $file = $dir."/".$val;
                                $paper[$ar]['pdf_url'] = str_replace("/home/ashvithtechlabs/public_html/epapersnews/","", $file);
                                $thumb = str_replace(".pdf",".jpg", $paper[$ar]['pdf_url']);
                                if(file_exists('../'.$thumb))
                                {
                                
                                    $paper[$ar]['thumb_url'] = $thumb;
                                }
                                else
                                {
                                    $paper[$ar]['thumb_url'] = $getCity['image'];
                                }
                                $ar++;
                            }
                        }
                        if($paper != null)
                        {
                            $list["pdf"] = $paper;  
                        }
                        else
                        {
                            $list["pdf"] = [];  
                        }
                    }
                }
                if($cityName != null)
                {
                    $list["cityName"] = $cityName;
                    $city[] = $list;
                }
                else
                {
                    $list["cityName"] = "";
                    $city[] = $list;
                }
                
            }
        echo json_encode(array("city" => $city, "ResponseCode" => "1", "Result" => "True"));
    }
    else
    {
        echo json_encode(array("city" => [], "ResponseCode" => "2", "Result" => "False"));
    }
?>