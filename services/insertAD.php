<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $headers = array("Content-Type:multipart/form-data");
 
    $adTitle = $_REQUEST["adTitle"];
    $adEmail = $_REQUEST["adEmail"];
    $adMobile = $_REQUEST["adMobile"];
    $adDescription = $_REQUEST["adDescription"];
    $adFromDate = $_REQUEST["adFromDate"];
    $adToDate = $_REQUEST["adToDate"];
    $adPhoto = $_FILES['adPhoto']['name'];
	$adStatus = 0;

	$path = "images/advertisement/".$adPhoto;

	$insertAd = array("adTitle"=>$adTitle, "adEmail"=>$adEmail, "adMobile"=>$adMobile, "adDescription"=>$adDescription, "adFromDate"=>$adFromDate, "adToDate"=>$adToDate, "adPhoto"=>$path, "adStatus"=>$adStatus);
	$Ad = $cnn -> insertRec($insertAd, "ad_master");

	$path1 = "../images/advertisement/";
	move_uploaded_file($_FILES['adPhoto']['tmp_name'], $path1.$adPhoto);
	
	if($Ad > 0)
	{		
		echo json_encode(array("ResponseCode"=>"1","ResponseMsg"=> "Advertise Insert Successfully.","Result"=>"true"));
	}
	else
	{
		echo json_encode(array("ResponseCode"=>"2","ResponseMsg"=> "Could Not Insert Advertise.","Result"=>"true"));
	}
?>