<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $headers = array("Content-Type:multipart/form-data");
  		
  	$supplement = array();
  	
  	$date=date('Y-m-d');
	$selectSupplement = $cnn -> getrows("SELECT supnews_master.snPdf,supnews_master.snUploadDate,sup_master.supName FROM supnews_master left join sup_master on supnews_master.supID=sup_master.supID  WHERE supnews_master.snUploadDate = '$date' and supnews_master.snStatus='1' ");
	
	if($selectSupplement > 0)
	{
		while($getSupplement = mysqli_fetch_assoc($selectSupplement))
		{
			$supplement[] = $getSupplement;
		}
		echo json_encode(array("supplement" => $supplement, "ResponseCode" => "1", "Result" => "True"));
	}
	else
	{
		echo json_encode(array("supplement" => [], "ResponseCode" => "2", "Result" => "False"));
	}
?>