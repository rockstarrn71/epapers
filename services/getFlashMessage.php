<?php
header('Access-Control-Allow-Origin: *');
include("../include/config.php");
$cnn = new connection();
$headers = array("Content-Type:multipart/form-data");

$flashMessage = $cnn->getrows("SELECT c.* FROM configs c WHERE c.config_key = 'flash_message' LIMIT 1");
$getFlashMessage = mysqli_fetch_array($flashMessage);
echo json_encode(array("FlashMessage" => $getFlashMessage['config_value'], "ResponseCode" => "1", "Result" => "True"));
?>