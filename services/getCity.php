<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $headers = array("Content-Type:multipart/form-data");
  		
  	$city = array();
  	$stateID = $_REQUEST['stateID'];
	$selectCity = $cnn -> getrows("SELECT city_master.*,state_master.* FROM city_master INNER JOIN state_master ON city_master.stateID = state_master.stateID WHERE state_master.stateID = '$stateID'");
	if($selectCity > 0)
	{
		while($getCity = mysqli_fetch_assoc($selectCity))
		{
			$city[] = $getCity;
		}
		echo json_encode(array("city" => $city, "ResponseCode" => "1", "Result" => "True"));
	}
	else
	{
		echo json_encode(array("city" => [], "ResponseCode" => "2", "Result" => "False"));
	}
?>