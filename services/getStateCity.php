<?php
    header('Access-Control-Allow-Origin: *');
    include("../include/config.php");
    $cnn = new connection();
    $headers = array("Content-Type:multipart/form-data");
  		
  	$state = array();
  	$city = array();
	$selectState = $cnn -> getrows("SELECT * FROM state_master");
	if($selectState > 0)
	{
		while($getState = mysqli_fetch_assoc($selectState))
		{
			$stateID = $getState['stateID'];
			//$state[] = $getState;
			$selectCity = $cnn -> getrows("SELECT *FROM city_master WHERE stateID = '$stateID'");
			$city = [];
			while ($getCity = mysqli_fetch_assoc($selectCity))
			{
				$city[] = $getCity;
			}
			$getState['city'] = $city;
			$state[] = $getState;
		}
		echo json_encode(array("state" => $state, "ResponseCode" => "1", "Result" => "True"));
	}
	else
	{
		echo json_encode(array("state" => [], "ResponseCode" => "2", "Result" => "False"));
	}
?>