<?php
  //include("include/config.php");
  date_default_timezone_set('Asia/Kolkata'); 
  include("include/session.php");
  ini_set('display_errors',1);
  //$cnn = new connection();
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - News</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        News
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add Short News Categories</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<form name="state" id="state" method="POST" action="add_categories.php">


                       <div class="form-group row">
						  	<label for="cityName" class="col-sm-2 col-form-label">New Category</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="text" id="title" name="name" placeholder="New Category" required="">
						  	</div>
						</div>
						
				
					
					<center>
						<button type="submit" id="save" name="save" class="btn btn-success btn-flat" style="font-size: 14px;">Submit <i class="fa fa-fw fa-arrow-circle-o-right"></i></button>
					</center>
				</form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- /.row -->

      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">View News</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
				<thead>
					<tr>
						<th>S.No</th>
						<th>Category</th>
					</tr>
				</thead>
				<tbody>
					<?php
						include "connection.php";
						$sql = "select * from categories";
						$res = mysqli_query($conn,$sql);
						
						$i = 1;
						while($row = mysqli_fetch_array($res))
						{ 
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $row['name']; ?></td>
						<td>
							<a href="edit_categories.php?id=<?php echo $row['id']; ?>" class="btn btn-danger btn-flat" style="font-size: 14px;">Edit <i class="fa fa-fw fa-arrow-circle-o-right"></i></a>
							
							<!--<a href="addit.php?id=<?php echo $row['id']; ?>&deleteCity=deleteCity" class="btn btn-primary btn-flat" style="font-size: 14px;">Delete <i class="fa fa-fw fa-arrow-circle-o-right"></i></a>-->
						     <button type="button" value="Delete" onclick="delete_cat('<?php echo $row['id']; ?>')" class="btn btn-info btn-flat" style="font-size: 14px;">Delete <i class="fa fa-fw fa-arrow-circle-o-right"></i></button>
						
						
						</td>
					</tr>
					<?php $i++; } ?>
					
				</tfoot>
			</table>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	<!-- Sparkline -->
	<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>

	<!-- This is data table -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- apro_admin for Data Table -->
	<script src="js/pages/data-table.js"></script>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	
	
	<script>
  $( function() {
    $("#datepicker").datepicker();
  } );
</script>


<script>

function delete_cat(id)
{
	
	if(confirm('Do You really want to delete this record'))
	{
	   
		$.post('delete_category.php',{id:id},function(data)
		{
		   
		    
		if(data==1)
		{
		alert('Category deleted successfully');
		window.location='add_categories.php';
		}
		else 
		{
		alert('Unable to delete , Try again');
		}
		
		
		});
	}

} 


</script>

</body>
</html>

<?php



$conn = mysqli_connect("localhost", "ashvitht_enewson", "Ashvith@99", "ashvitht_enews");

if(isset($_POST['save']))
{
 
 
 $name = $_POST['name'];
 
  $sql = "insert into categories(name) values('".$name."')";
 
 $res = mysqli_query($conn,$sql);
 
 
 
 
 if($res)
 {
     echo "<script>alert('Category Added Succesfully');window.location = 'add_categories.php';</script>";
 }
 else
 {
     echo "<script>alert('Something Went Wrong');window.location = 'add_categories.php';</script>";
 }
 
 
 
 
    
}
?>