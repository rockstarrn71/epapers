<?php
  //include("include/config.php");
  date_default_timezone_set('Asia/Kolkata'); 
  include("include/session.php");
  ini_set('display_errors',1);
  
  
  $conn = mysqli_connect("localhost", "ashvitht_enewson", "Ashvith@99", "ashvitht_enews");
  
  $id = $_REQUEST['id'];
  $sqll = "select * from media where id = '".$id."'";
  $ress = mysqli_query($conn,$sqll);
  while($red = mysqli_fetch_array($ress))
  {
      
  ?>
  
  
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - News</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        News
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Add City</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
            	<form name="state" id="state" method="POST" action="updatemedia.php" enctype="multipart/form-data">
            	    <input class="form-control" type="hidden" id="cityID" name="id" value="<?php echo $red['id']; ?>">
            		<input class="form-control" type="hidden"  name="old_video" value="<?php echo $red['video']; ?>">
            		<input class="form-control" type="hidden"  name="old_image" value="<?php echo $red['image']; ?>">


                       <div class="form-group row">
						  	<label for="cityName" class="col-sm-2 col-form-label">Title</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="text" id="name" name="name" placeholder="Title" value="<?php echo $red['name']; ?>" required="">
						  	</div>
						</div>
						
						
						<div class="form-group row">
						  	<label for="cityName" class="col-sm-2 col-form-label">Description</label>
						 	<div class="col-sm-10">
								<textarea class="form-control" row = "5" id="description" name="description"><?php echo $red['description']; ?></textarea>
						  	</div>
						</div>
						
				
                	<div class="form-group row">
					    <label for="stateName" class="col-sm-2 col-form-label">Category Selection:</label>
					    <div class="col-sm-10">
						    <select class="form-control" name="catselect" id="catselect" required>
								<option value="">---Select----</option>
								<?php	
							
							$sql = "select * from categories";
							$res = mysqli_query($conn,$sql);
							while($row = mysqli_fetch_array($res))
							{
							    if($row['id'] == $red['catselect'])
							    {
							    ?>
							    <option value="<?php echo $row['id']; ?>" selected><?php echo $row['name']; ?></option>
							    <?php
							}
							else
							{
							?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
							<?php
							}
							}
							?>
	                  		</select>
					  	</div>
					</div>
					
					
					
					<div class="form-group row">
					    <label for="stateName" class="col-sm-2 col-form-label">News:</label>
					    <div class="col-sm-10">
						    <select class="form-control" name="image_or_video" id="image_or_video" onchange="get_data()" required>
								<option value="">---Select----</option>
								<option value="1"<?php if($red['image_or_video'] == '1'){ echo 'selected';} ?>>Image</option>
								<option value="2"<?php if($red['image_or_video'] == '2'){ echo 'selected';} ?>>Video</option>
						
	                  		</select>
					  	</div>
					</div>
					
					
					<?php if($red['image_or_video'] == '1'){ ?>
					<div class="form-group row" id="thumb">
					  <label for="image" class="col-sm-2 col-form-label">Image</label>
					  <div class="col-sm-10">
						<input class="form-control" type="file" id="image" name="image">
						<img src="<?php echo $row['image']; ?>" height="50" width="50">
					  </div>
					</div>
					<?php } else { ?>
					<div class="form-group row" style="display:none;" id="thumb">
						  	<label for="image" class="col-sm-2 col-form-label">Image</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="file" id="image" name="image">
						  	</div>
					</div>
					
					<?php } ?>
					
					
					<?php if($red['image_or_video'] == '2'){ ?>
					<div class="form-group row" id="img">
					  <label for="image" class="col-sm-2 col-form-label">Video</label>
					  <div class="col-sm-10">
						<input class="form-control" type="file" id="video" name="video">
						<?php echo $red['video']; ?>
					  </div>
					</div>
					
					<?php } else { ?>
					<div class="form-group row" style="display:none;" id="img">
						  	<label for="image" class="col-sm-2 col-form-label">Video</label>
						 	<div class="col-sm-10">
								<input class="form-control" type="file" id="video" name="video">
						  	</div>
					</div>
					
					<?php } ?>
					
					
					
					<center>
						<button type="submit" id="addgallery" name="save" class="btn btn-success btn-flat" style="font-size: 14px;">Submit <i class="fa fa-fw fa-arrow-circle-o-right"></i></button>
					</center>
				</form>
				
				<?php
  }
  ?>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!-- /.row -->

      
      <!-- /.box -->
      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	<!-- Sparkline -->
	<script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>

	<!-- This is data table -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
    
    <!-- start - This is for export functionality only -->
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
	
	<!-- apro_admin for Data Table -->
	<script src="js/pages/data-table.js"></script>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	
	
	<script>
  $( function() {
    $("#datepicker").datepicker();
  } );
</script>


<script>

function get_data()
{
    var data  = $("#image_or_video").val();
    if(data == '1')
    {
        $("#thumb").show();
        $("#img").hide();
    }
    else
    {
        $("#img").show();
         $("#thumb").hide();
    }
}

</script>

<script>

function delete_this(id)
{
	
	if(confirm('Do You really want to delete this record'))
	{
	   
		$.post('delete_media.php',{id:id},function(data)
		{
		   
		    
		if(data==1)
		{
		alert('Data deleted successfully');
		window.location='media.php';
		}
		else 
		{
		alert('Unable to delete , Try again');
		}
		
		
		});
	}

} 


</script>

</body>
</html>

<?php



$conn = mysqli_connect("localhost", "ashvitht_enewson", "Ashvith@99", "ashvitht_enews");

if(isset($_POST['save']))
{
 
 
 $name = $_POST['name'];
 $description = $_POST['description'];
 $catselect = $_POST['catselect'];
 $image_or_video = $_POST['image_or_video'];
 
 $ddd = date('Y-m-d',strtotime($_POST['dddd']));
  
 
  

 
 
 	$image = time().$_FILES['image']['name'];
		

		$path = "galleryimages/".$image;
 
 
        $path1 = "galleryimages/";
		move_uploaded_file($_FILES['image']['tmp_name'], $path1.$image);
		
 
 $thumbnail = time().$_FILES['video']['name'];
		

		$path2 = "galleryimages/".$thumbnail;
 
 
        $path3 = "galleryimages/";
		move_uploaded_file($_FILES['video']['tmp_name'], $path3.$thumbnail);
  
 
 if($_FILES['image']['name'] !='')
 {
     $img = $path;
 }
 else
 {
     $img = '';
 }
 
 
 if($_FILES['video']['name'] !='')
 {
     $vid = $path2;
 }
 else
 {
     $vid = '';
 }
 
  $sql = "insert into media(name,description,catselect,image_or_video,image,video,dddd) values('".$name."','".$description."','".$catselect."','".$image_or_video."','".$img."','".$vid."','".$ddd."')";
 
 $res = mysqli_query($conn,$sql);
 
 
 
 
 if($res)
 {
     echo "<script>alert('News Added Succesfully');window.location = 'media.php';</script>";
 }
 else
 {
     echo "<script>alert('Something Went Wrong');window.location = 'media.php';</script>";
 }
 
 
 
 
    
}
?>