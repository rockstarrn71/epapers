<?php
  include("include/config.php");
  include("include/session.php");
  $cnn = new connection();
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>E-paper - View News</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
	
	<!-- font awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
	
	<!-- ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	
	<!-- apro_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="css/skins/_all-skins.css">

	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	  <link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
	  
     
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <!-- header -->
  <?php include("include/header.php"); ?>
  <!-- End header -->
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include("include/leftbar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        OtherNews
        <small>Control panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">State</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
	  <?php
	  
	  $selectState = $cnn -> getrows("SELECT *,DATE_FORMAT(date,'%d %b, %Y') as uploaddate FROM Othernews_master order by otherID desc");
		
		while($getState = mysqli_fetch_array($selectState))
		{
			
		
	  
	  ?>
	  
        <div class="col-md-12 col-lg-3">
          <div class="box box-default">
		 
		  <?php
				if($getState['Type'] == 'image')
				{	
				?>
				 <img class="card-img-top img-responsive" src="othernewsimage/<?php Echo $getState['fileUpload'];?>" alt="News image ">
				<?php } else if($getState['Type'] == 'video')

					{?>	
				<video class="card-img-top img-responsive" controls="controls">
				<source id="sot_1" src="othernewsvideo/<?php Echo $getState['fileUpload'];?>" type="video/mp4">
				<object data="othernewsvideo/<?php Echo $getState['fileUpload'];?>" ></object>
				</video>
				<?php }?>
            <div class="box-body">            	
				
				 <h4 class="card-title"><?php Echo $getState['Title'];?> <small style="float: right;font-family: -webkit-body;"><?php Echo $getState['uploaddate'];?></small></h4>
				<p class="card-text"><?php Echo $getState['Description'];?></p>
				<?php
				if($getState['status'] == 1)
				{	
				?>
					<a href="addOthernewsScript.php?otherID=<?php Echo $getState['otherID'];?>&status=deactive" class="btn btn-primary" style="font-size: 14px;">Deactive</a>
				<?php } else if($getState['status'] == 0) {?>
					<a href="addOthernewsScript.php?otherID=<?php Echo $getState['otherID'];?>&status=active" class="btn btn-info" style="font-size: 14px;">Active</a>
				<?php } ?>
				<a href="addOthernewsScript.php?oID=<?php Echo $getState['otherID'];?>" class="btn btn-danger" style="font-size: 14px;">Delete</a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <?php } ?>
      </div>
	</section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
  <?php include("include/footer.php"); ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	  
	<!-- jQuery 3 -->
	<script src="assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- popper -->
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	

	<!-- DataTables -->
	<script src="assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	
	<!-- Slimscroll -->
	<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- apro_admin App -->
	<script src="js/template.js"></script>
	
	<!-- apro_admin for demo purposes -->
	<script src="js/demo.js"></script>

	<!-- apro_admin for Data Table -->
	<script src="js/pages/data-table.js"></script>

	
	
</body>

</html>
